# README #

This plugin connects to the Atira Pure API and pulls out the profile information, which it caches in local tables. It makes available an API which serves JSON to the Pure Client plugin. Set up a dedicated WordPress install and install this plugin to make it act as your proxy server.

### Dependencies ###
[WP REST API](http://v2.wp-api.org/)

## Installation ###
1. [Download the repository](https://bitbucket.org/hssweb/pure-proxy/downloads). 
2. Unzip to extract the files and rename the folder so that you have a folder called pure-proxy. Zip this folder, naming it pure-proxy.zip.
3. Install using the Add Plugin option in your site’s dashboard. The plugin creates tables and options in the database so it’s important to install and uninstall using this method.

### Pure Client plugin ###
The Pure Client plugin can be found [here](https://bitbucket.org/hssweb/pure-client). It should be installed on a separate WordPress instance.

### Contribution ###
The plugin is maintained by the CAHSS Web Team at the University of Edinburgh. Contributions are welcome. Contact gavin.maxwell@ed.ac.uk.