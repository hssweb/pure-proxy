<?php
/*
Plugin Name: Pure Proxy
Description: This plugin connects to the Atira Pure API and pulls out the profile information, which it caches in local tables. It makes available an API serving JSON to the Pure Client plugin.
Author: Gavin Maxwell, CAHSS Web Team, The University of Edinburgh
Version: 1.0
Author URI: http://www.ed.ac.uk/cahss/web-team
License: GPL3
*/

define('PUREPROXY_PATH', plugin_dir_path(__FILE__) );
$pure_proxy_db_version = "1.0";
$pure_proxy_current_db_version = "";

require_once(PUREPROXY_PATH.'/libraries/guzzle.phar');
require_once(PUREPROXY_PATH.'/include/mappings.php');
require_once(PUREPROXY_PATH.'/pure-proxy-admin-menu.php');
require_once(PUREPROXY_PATH.'/models/Clipping.class.php');
require_once(PUREPROXY_PATH.'/models/Person.class.php');
require_once(PUREPROXY_PATH.'/models/Project.class.php');
require_once(PUREPROXY_PATH.'/models/Publication.class.php');
require_once(PUREPROXY_PATH.'/models/Organisation.class.php');

/* add settings link to plugins page */
function pure_proxy_settings_link($links) { 
  $settings_link = '<a href="admin.php?page=pure-proxy-menu">Settings</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'pure_proxy_settings_link' );





