<?php
/*
 * This script should be called overnight from a cron. It could also be called by pressing a button in the control panel.
 */
if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( '../../../wp-load.php');
}
require_once(PUREPROXY_PATH.'/libraries/guzzle.phar');
require_once(PUREPROXY_PATH.'/models/Clipping.class.php');
require_once(PUREPROXY_PATH.'/models/Person.class.php');
require_once(PUREPROXY_PATH.'/models/Project.class.php');
require_once(PUREPROXY_PATH.'/models/Publication.class.php');
require_once(PUREPROXY_PATH.'/models/Organisation.class.php');

// check that the key defined in options matches the one in the query string
$qs_key = $_REQUEST['key'];
$cron_options = get_option('pure_proxy_cron_options');
$options_key = $cron_options['pure_proxy_cron_key_settings'];
$debug = $cron_options['pure_proxy_cron_debug_settings'];

function debug($diagnostic, $msg = "") {
  global $debug;
  if (! $debug) return;
  
  $backtrace = debug_backtrace();
  if (isset($backtrace[1])) {
    echo "METHOD: ".$backtrace[1]['class'].":".$backtrace[1]['function'];
    echo ", LINE: ".$backtrace[1]['line'];
  }
  echo "<br>";
  if ($msg != "") {
    echo $msg.": ";
  }
  if (is_array($diagnostic)) {
      echo "<pre>";
      print_r($diagnostic);
      echo "</pre>";
  } else {
      echo $diagnostic;
      echo "<br>";
  }
  echo "<hr>";
}

if ($qs_key == $options_key) {
	Person::get_pure_persons();
	Organisation::get_pure_organisations(1);
	echo "Finished updating local cache";
} else {
	echo "Keys do not match";
}
