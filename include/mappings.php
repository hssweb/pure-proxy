<?php if(!defined('PUREPROXY_PATH')){die('Direct access not permitted');}

// whitelist of tables available via API
$pure_proxy_table_list = array(
  'clipping',
  'clipping_person',
  'clipping_reference',
  'organisation',
  'organisation_association',
  'organisation_information',
  'person',
  'person_information',
  'project',
  'project_keywords',
  'project_organisation',
  'project_person',
  'project_project_association',
  'project_publication_association',
  'publication',
  'publication_document',
  'publication_keywords',
  'publication_organisation',
  'publication_person',
  'publication_publication_association',
  'staff_organisation_association'  
);
// these can be a single value or comma-separated list
$pure_proxy_allowed_parameters = array(
  'organisationuuid',
  'personuuid',
  'publicationuuid'
);

/**
 * Grab all data from specified table
 *
 * @return json encoded content or null if none.
 */
function return_table( $data ) {
  global $wpdb, $_SERVER, $pure_proxy_table_list, $pure_proxy_allowed_parameters;

  $query_append = "";
  $query_conditions = "";
  $parameters = $data->get_params();
  if ($parameters != null) {
    foreach ($parameters as $key=>$val) {
      // need better escaping - this just does the basics
      $val = str_replace(" ", "", $val);
      $val = str_replace("'", "", $val);
      $val = str_replace("\"", "", $val); 
      if (in_array($key, $pure_proxy_allowed_parameters)) {
        $vals = explode(",", $val);
        foreach ($vals as $v) {
          $query_conditions.= " OR ".$key."='".$v."'";
        }
      }
    }
    $query_append = " WHERE 1=2".$query_conditions;
  }  
   
  $results = null;
  $path = strtok($_SERVER["REQUEST_URI"],'?');
  $path_components = explode("/", $path);
  $table = $path_components[4];
  if (in_array($table, $pure_proxy_table_list)) {
    $sql = "SELECT * FROM " . $wpdb->prefix . "pure_profiles_".$table.$query_append;
    $results = $wpdb->get_results( $sql ); 
  }

  return $results;
}


add_action( 'rest_api_init', function () {
      
  global $pure_proxy_table_list;      
  foreach ($pure_proxy_table_list as $t) {
    register_rest_route( 'pure-proxy/v1', '/'.$t, array(
        'methods' => 'GET',
        'callback' => 'return_table',
    ) );     
  }
     
} );





