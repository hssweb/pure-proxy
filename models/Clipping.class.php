<?php if(!defined('PUREPROXY_PATH')){die('Direct access not permitted');}
require_once(PUREPROXY_PATH.'/libraries/guzzle.phar');
use Guzzle\Http\Client;
		
class Clipping {

	public static function get_clippings($type="Person", $uuid) {
		global $wpdb, $client, $username, $password;
				
		/* get all clippings */
		$request = $client->get('/ws/rest/clipping?rendering=xml_long&associated'.$type.'Uuids.uuid='.$uuid);
		$request->setAuth($username, $password);
		try {
			$response = $request->send();
		} catch (Guzzle\Http\Exception\BadResponseException $e) {
			echo $e->getMessage();
		}
		$xml = $response->xml();
		$namespaces = $xml->getNamespaces(true);
				
		$core = $xml->children($namespaces['core']);
			
		$retrieved = date("F j, Y, g:i a"); 

		for ($i=0; $i < $core->count; $i++) {
			$clip_attributes = $core->result->content[$i]->attributes();
			$clip_uuid = (string)$clip_attributes['uuid'];		
			$clipping = $core->result->content[$i]->children($namespaces['stab1']);		
			$clip_title = $clipping->title;
			$clip_date = $clipping->date;			
			if ($type=="Person") {
				$person_uuid = $uuid;
				$organisation_uuid = "";
			} else if ($type=="Organisation") {
				$person_uuid = "";
				$organisation_uuid = $uuid;
			}		
		
			/* insert clipping into table */
			$results = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_clipping WHERE clippinguuid='" . $clip_uuid . "' ");			
			if (count($results) == 0) {
				$table_name = $wpdb->prefix . "pure_profiles_clipping";
				$rows_affected = $wpdb->insert( $table_name, array( 
					'clippinguuid' => $clip_uuid, 			
					'title' => $clip_title, 
					'date' => $clip_date,
					'retrieved' => $retrieved
				));			
				
				/* references */
				
				
				
			} // end if (count($results) == 0)
			
			
			if ($type == 'Person') {
				$results = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_clipping_person WHERE personuuid='" . $person_uuid . "' ".
					"AND clippinguuid='".$clip_uuid."'");			
				if (count($results) == 0) {
					$table_name = $wpdb->prefix . "pure_profiles_clipping_person";  
					$rows_affected = $wpdb->insert( $table_name, array( 
						'clippinguuid' => $clip_uuid, 			
						'personuuid' => $person_uuid,
						'title' => $clip_title, 
						'retrieved' => $retrieved
					));	
				}
			}
			
		}
	}
}