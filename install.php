<?php if(!defined('PUREPROXY_PATH')){die('Direct access not permitted');}

function pure_proxy_install() {
   global $wpdb, $pure_proxy_db_version;
   
   	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

   $table_name = $wpdb->prefix . "pure_profiles_person";  
   $sql = "CREATE TABLE $table_name (
	  personuuid varchar(40),
	  employeeid mediumint(9),
	  portalurl VARCHAR(255),	    
	  firstname tinytext NOT NULL,
	  lastname tinytext NOT NULL,	
	  callnamefirstname VARCHAR(150),
	  callnamelastname VARCHAR(150),	  
	  title VARCHAR (30),
	  photos TEXT,
	  retrieved varchar(30),	  
	  UNIQUE KEY personuuid (personuuid)
		);";
	dbDelta( $sql );
	
   $table_name = $wpdb->prefix . "pure_profiles_person_information";  
   $sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  personuuid varchar(40),
	  title VARCHAR(255),
	  content TEXT,
	  retrieved varchar(30),	  
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );	
	
	$table_name = $wpdb->prefix . "pure_profiles_organisation";  
	$sql = "CREATE TABLE $table_name (
	  organisationuuid varchar(40),
	  organisationid VARCHAR(20),
	  organisationname tinytext NOT NULL,
	  organisationshortname tinytext NOT NULL,	
	  portalurl VARCHAR(255),	  
	  website VARCHAR(255),
	  phone VARCHAR (100),
	  email VARCHAR (100),	  
	  retrieved varchar(30),
	  UNIQUE KEY organisationuuid (organisationuuid)
		);";
	dbDelta( $sql );	
	
   $table_name = $wpdb->prefix . "pure_profiles_organisation_information";  
   $sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  organisationuuid varchar(40),
	  title VARCHAR(255),
	  content TEXT,
	  retrieved varchar(30),	  
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );	
	
	$table_name = $wpdb->prefix . "pure_profiles_organisation_association";  
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  personuuid varchar(40),
	  employeeid mediumint(9),
	  organisationuuid VARCHAR(40),
	  organisationid VARCHAR(20),
	  organisationname TINYTEXT NOT NULL,
	  organisationshortname tinytext NOT NULL,	  
	  organisationtypeclassificationid VARCHAR(255),
	  organisationtypeclassification VARCHAR(255) DEFAULT '' NOT NULL,	  
	  employmenttypeid mediumint(9),
	  employmenttype VARCHAR(255),
	  retrieved varchar(30),	  
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );		
	
	$table_name = $wpdb->prefix . "pure_profiles_staff_organisation_association";  
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  personuuid varchar(40),
	  employeeid mediumint(9),
	  organisationuuid VARCHAR(40),
	  jobdescription VARCHAR (255),
	  retrieved varchar(30),	  
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );	
  
  $table_name = $wpdb->prefix . "pure_profiles_person_retrieved_by_organisation";
  $sql = "CREATE TABLE $table_name (
    personuuid VARCHAR(40),
    organisationuuid VARCHAR(40),
    retrieved VARCHAR(30)
    );";
    dbDelta( $sql );
	
 	$table_name = $wpdb->prefix . "pure_profiles_project";  
	$sql = "CREATE TABLE $table_name (
	  uuid varchar(40),
	  title VARCHAR(255),
	  portalurl VARCHAR(255),
	  acronym VARCHAR(100),
	  description TEXT NOT NULL,
	  laymansdescription TEXT NOT NULL,
	  projecturl VARCHAR(255),
	  startdate VARCHAR(255),
	  finishdate VARCHAR(255),
	  projectstatusid VARCHAR(10),
	  projectstatus VARCHAR(255),
	  typeclassificationid VARCHAR(20),
	  typeclassification VARCHAR(255),
	  fundingorganisation VARCHAR(255),
	  keyfindings TEXT NOT NULL,
	  parentproject VARCHAR(40),
	  retrieved varchar(30),
	  UNIQUE KEY uuid (uuid)
		);";
	dbDelta( $sql );	
	
	$table_name = $wpdb->prefix . "pure_profiles_project_keywords";
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  projectuuid varchar(40),
	  keywordcontainerid varchar (40),
	  userdefinedkeywordid varchar (40),
	  keyword varchar (255),
	  retrieved varchar(30),
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );	
 
  	$table_name = $wpdb->prefix . "pure_profiles_project_person";  
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  personuuid varchar(40),
	  projectuuid varchar(40),
	  title VARCHAR(255),
	  firstname VARCHAR(255),
	  lastname VARCHAR(255),
	  callnamefirstname VARCHAR(255),
	  callnamelastname VARCHAR(255),
	  rolename VARCHAR (255),
	  retrieved varchar(30),
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );	
	
	$table_name = $wpdb->prefix . "pure_profiles_project_organisation";  
	$sql = "CREATE TABLE $table_name (
	  projectuuid varchar(40),
	  title TEXT NULL DEFAULT NULL,
	  organisationuuid VARCHAR(40),
	  organisationid VARCHAR(50),
	  organisationname VARCHAR(255),
	  organisationshortname VARCHAR(100),
	  orgtypeclassification VARCHAR(255),
	  orgtypeclassificationid VARCHAR(40),	  
	  retrieved varchar(30)
		);";
	dbDelta( $sql );
	
	
	$table_name = $wpdb->prefix . "pure_profiles_project_project_association";
	$sql = "CREATE TABLE $table_name (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `proj1_uuid` varchar(40) NOT NULL,
	  `proj1_title` text NOT NULL,
	  `proj2_uuid` varchar(40) NOT NULL,
	  `proj2_title` text NOT NULL,
	  `retrieved` varchar(30) NOT NULL,
	  PRIMARY KEY (`id`)
	);";
	dbDelta( $sql );


	$table_name = $wpdb->prefix . "pure_profiles_project_publication_association";  
	$sql = "CREATE TABLE $table_name (		
		id int(11) NOT NULL AUTO_INCREMENT,
		project_uuid varchar(40) NOT NULL,
		project_title varchar(255) NOT NULL,
		publication_uuid varchar(40) NOT NULL,
		publication_title varchar(255) NOT NULL,
		retrieved varchar(30) NOT NULL,
		UNIQUE KEY id (id)
	);"; 
	dbDelta( $sql );		
 
 	$table_name = $wpdb->prefix . "pure_profiles_publication";  
	$sql = "CREATE TABLE $table_name (
	  uuid varchar(40),
	  title VARCHAR(255),
	  subtitle TEXT,
	  portalurl VARCHAR(255),	  
	  abstract TEXT,
	  typeclassification VARCHAR(255),
	  originallanguage VARCHAR(50),
	  state VARCHAR(50),
	  dois TEXT,
	  printisbns VARCHAR(255),
	  eisbns VARCHAR(255),
	  numberofpages VARCHAR(50),
	  publicationyear varchar(10),
	  publicationmonth varchar(10),
	  publicationday varchar(10),
	  volume VARCHAR(255),
	  journalnumber VARCHAR(50),
	  journalpages VARCHAR(255),
	  journaltitle VARCHAR(255),
	  journalissn VARCHAR(255),
	  bibliographicalnote TEXT,
	  vancouver TEXT,
	  harvard TEXT,	
	  peerreview VARCHAR(10),
	  retrieved varchar(30),
	  UNIQUE KEY uuid (uuid)
		);";
	dbDelta( $sql );	
	
 	$table_name = $wpdb->prefix . "pure_profiles_publication_document";  
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  publicationuuid varchar(40),
	  url VARCHAR(255),
	  retrieved varchar(30),
	  UNIQUE KEY id (id)
	    );";
	dbDelta( $sql );	
	
	
	$table_name = $wpdb->prefix . "pure_profiles_publication_keywords";
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  publicationuuid varchar(40),
	  keywordcontainerid varchar (40),
	  userdefinedkeywordid varchar (40),
	  keyword varchar (255),
	  retrieved varchar(30),
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );		
	
	
	$table_name = $wpdb->prefix . "pure_profiles_publication_person";  
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  personuuid varchar(40),
	  publicationuuid VARCHAR(40),
	  title VARCHAR(255),
	  firstname VARCHAR(255),
	  lastname VARCHAR(255),
	  personrole VARCHAR(255),
	  retrieved varchar(30),	  
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );		

	$table_name = $wpdb->prefix . "pure_profiles_publication_organisation";  
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  organisationuuid varchar(40),
	  publicationuuid VARCHAR(40),
	  title VARCHAR(255),
	  organisationname VARCHAR(255),
	  organisationid VARCHAR(100),
	  organisationshortname VARCHAR(100),
	  orgtypeclassification VARCHAR(255),
	  orgtypeclassificationid VARCHAR(40),
	  retrieved varchar(30),	  
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );		
	
	$table_name = $wpdb->prefix . "pure_profiles_publication_publication_association";
	$sql = "CREATE TABLE $table_name (
		id int(11) NOT NULL AUTO_INCREMENT,
		pub1_uuid varchar(40) NOT NULL,
		pub1_title text NOT NULL,
		pub2_uuid varchar(40) NOT NULL,
		pub2_title text NOT NULL,
		retrieved varchar(30) NOT NULL,
		UNIQUE KEY (`id`)
		);";
	dbDelta( $sql );		

 
	$table_name = $wpdb->prefix . "pure_profiles_clipping";  
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  clippinguuid VARCHAR(40),
	  title VARCHAR(255),
	  date VARCHAR(100),
	  retrieved varchar(30),	  
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );	 
	
	$table_name = $wpdb->prefix . "pure_profiles_clipping_reference";  
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  clippinguuid VARCHAR(40),
	  title VARCHAR(255),
	  medium VARCHAR(100),
	  place VARCHAR(150),
	  internationalreference VARCHAR(20),
	  retrieved varchar(30),	  
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );	 	
 
 	$table_name = $wpdb->prefix . "pure_profiles_clipping_person";  
	$sql = "CREATE TABLE $table_name (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  personuuid varchar(40),
	  clippinguuid VARCHAR(40),
	  title VARCHAR(255),
	  retrieved varchar(30),	  
	  UNIQUE KEY id (id)
		);";
	dbDelta( $sql );	
 
	add_option( "pure_proxy_db_version", $pure_proxy_db_version );
}