<?php if(!defined('PUREPROXY_PATH')){die('Direct access not permitted');}
require_once(PUREPROXY_PATH.'/libraries/guzzle.phar');
use Guzzle\Http\Client;
		
class Project {

	public static function get_projects($type="Person", $uuid) {
		global $wpdb, $client, $username, $password;
				
				
		$project_uuids = Project::get_pure_projects_uuids($type, $uuid); // get array of project UUIDs
		if (count($project_uuids) == 0 ) return;		

		/* get projects by UUID, in batches of 20 */
		$batch = array();		
		$num_projects = count($project_uuids);
		for ($i=0; $i< $num_projects; $i++ ) {
			if (trim($project_uuids[$i]) != "") {
				$batch[] = $project_uuids[$i];
			}
		
			if (($i>0) && ((($i+1) % 20) == 0) ) { 

				// make call
				Project::get_pure_projects_batch($batch);
				
				// make a new batch
				$batch = array();
			}
				
		}
		/* do remaining batch */
		if (count($batch) > 0) { 
			Project::get_pure_projects_batch($batch);
		}				
		
	}
		
	public static function get_pure_projects_uuids($type, $uuid) {
		global $wpdb, $client, $username, $password;		
		
		/* get project count */
		$request = $client->get('/ws/rest/project?rendering=xml_short&associated'.$type.'Uuids.uuid='.$uuid.'&window.size=1');
		$request->setAuth($username, $password);
		try {
			$response = $request->send();
		} catch (Guzzle\Http\Exception\BadResponseException $e) {
			echo $e->getMessage();
		}
				
		$xml = $response->xml();
		$namespaces = $xml->getNamespaces(true);
		$core = $xml->children($namespaces['core']);	
		$result_count = $core->count;
		if ($result_count == 0) return;		
		$project_uuids = array();
		

		/* get project UUIDs, in batches of 20 */
		$batch_size = 20;
		for ($i =0; $i < $result_count; $i = $i+$batch_size) {
		
			$request = $client->get('/ws/rest/project?rendering=xml_short&associated'.$type.'Uuids.uuid='.$uuid.'&window.size='.$batch_size.'&window.offset='.$i);
			$request->setAuth($username, $password);
			try {
				$response = $request->send();
			} catch (Guzzle\Http\Exception\BadResponseException $e) {
				echo $e->getMessage();
			}
				
			$xml = $response->xml();
			$namespaces = $xml->getNamespaces(true);
			$core = $xml->children($namespaces['core']);	
			$num_batch_results = count($core->result->content);
			for ($j=0; $j < $num_batch_results; $j++) {
				$content_attributes = $core->result->content[$j]->attributes();
				$project_uuids[]  = (string)$content_attributes['uuid'];			
			}
		}	
	
		return $project_uuids;		
	
	}
		
	public static function get_pure_projects_batch($uuids) {
		global $wpdb, $client, $host, $username, $password;
		$num_uuids = count($uuids);
		if ($num_uuids == 0 ) return;
		
		$str_uuids = implode(",", $uuids);  		
		$request = $client->get('/ws/rest/project?rendering=xml_long&uuids.uuid='.$str_uuids.'&window.size='.$num_uuids);
		$request->setAuth($username, $password);
		try {
			$response = $request->send();
		} catch (Guzzle\Http\Exception\BadResponseException $e) {
			echo $e->getMessage();
		}
		$xml = $response->xml();
		$namespaces = $xml->getNamespaces(true);
				
		$core = $xml->children($namespaces['core']);
			
		$retrieved = date("F j, Y, g:i a"); 
		
		for ($i=0; $i < $core->count; $i++) {
			$p_core =  $core->result->content[$i]->children($namespaces['core']);
			$project = $core->result->content[$i]->children($namespaces['stab1']);
					
			$project_attributes = $core->result->content[$i]->attributes();
			$proj_uuid = (string)$project_attributes['uuid'];
			$portal_url = $p_core->portalUrl;
					
			// check if project already exists in database. if so, don't need to add
			$table_name = $wpdb->prefix . "pure_profiles_project";			
			$exists = $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE projectuuid='".$proj_uuid."'" );
			if (count($exists) > 0) {	
				continue; // exit current iteration only
			}		
		
			$title = $project->title->children($namespaces['core']);
			$proj_title = $title->localizedString;
			$the_description = "";
			if ($project->description) {
				$description = $project->description->children($namespaces['core']);
				$the_description = $description->localizedString;
			}
			$the_url = (isset($project->projectURL)) ? $project->projectURL : "";
			$start_finish_date = $project->startFinishDate->children($namespaces['extensions-core']);
			$start_date = ($start_finish_date->startDate) ? $start_finish_date->startDate : "";
			$finish_date = ($start_finish_date->endDate) ? $start_finish_date->endDate: "";
			$status_id = "";
			$the_status = "";
			if ($project->status) {
				$status_attributes = $project->status->attributes();
				$status_id = ((string)$status_attributes->id) ? (string)$status_attributes->id : "";
				$status = $project->status->children($namespaces['core']);
				$the_status = ( $status->term->localizedString) ? $status->term->localizedString : "";
			}
			$type_classification_attributes = $project->typeClassification->attributes();
			$type_classification_id = (string)$type_classification_attributes->id;
			$type_classification = $project->typeClassification->children($namespaces['core']);
			$the_type_classification = $type_classification->term->localizedString;

			$parent_project_uuid = "";
			if ($project->parentProject) {
				$parent_project_attributes = $project->parentProject->attributes();
				$parent_project_uuid = ((string)$parent_project_attributes->uuid) ? (string)$parent_project_attributes->uuid : "";
			}
			
			$the_laymans_description = "";
			$the_acronym = "";
			$funding_organisation = "";
			$the_key_findings = "";
			$the_associated_publications = "";

			if ($namespaces['stab']) {
				$stab = $core->result->content[$i]->children('stab', true); 
				
				
				if ($stab->laymansdescription) {
					$laymans_description = $stab->laymansdescription->children($namespaces['core']);
					$the_laymans_description = $laymans_description->localizedString;
				}
				
				if ($stab->acronym) {
					$the_acronym = $stab->acronym;
				}				

				if ($stab->projectFundings) {
					$project_fundings = $stab->projectFundings->children($namespaces['funding-template']);			
					if ($project_fundings) {
						$num_project_fundings = $project_fundings->count();
						for ($f=0; $f < $num_project_fundings; $f++) {
							$external_organisation = $project_fundings[$f]->fundingOrganisation->children($namespaces['externalorganisation-template']);
							$ext_org_name = $external_organisation->name;
							$funding_organisation.= $ext_org_name;
							if ($f < ($num_project_fundings-1)) $funding_organisation.= ",";
						}
					}
				}
				
				if ($stab->keyFindings) {
					$key_findings = $stab->keyFindings->children($namespaces['core']);
					if ($key_findings) {
						$the_key_findings = $key_findings->localizedString;
					}
				}				
				
				if ($stab->associatedPublications) {
					for ($p=0; $p < $stab->associatedPublications->publication->count(); $p++) {
					
						$pub_attributes = $stab->associatedPublications->publication[$p]->attributes();
						$pub_uuid = (string)$pub_attributes['uuid'];
						$pub_title = (string)$stab->associatedPublications->publication[$p]->title;
									
						// insert publication associations into table 
						$table_name = $wpdb->prefix . "pure_profiles_project_publication_association";
						$exists = $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE projectuuid='".$proj_uuid."' AND publicationuuid='".$pub_uuid."'" );
						if (count($exists) > 0) {			
							continue; // exit current iteration only
						}
						
						$rows_affected = $wpdb->insert( $table_name, array( 
							'projectuuid' => $proj_uuid,
							'project_title' => $proj_title, 					
							'publicationuuid' => $pub_uuid, 
							'publication_title' => $pub_title, 
							'retrieved' => $retrieved
						));					
					
					}
				}		
			}
			
			/* add associated projects (associatedtoproject or associatedfromproject? */
			/**
			if ($stab->associatedProjects) {
				$projectAssociations = $stab->associatedProjects->children($namespaces['project']);
				$projectAssociationsCount = count($projectAssociations);				
				for ($p=0; $p < $projectAssociationsCount; $p++) {

					$project = $projectAssociations[$p]->children($namespaces['project']);

					if ($project->project) {
						$proj2_attributes = $projectAssociations[$p]->project->attributes();
						$proj2_uuid = $proj2_attributes->uuid;
						if ($project->project->title) {					
							$project2_title = $project->project->title->children($namespaces['core']);
							$proj2_title = ($project2_title->localizedString) ? $project2_title->localizedString : ""; 
						}			
					} else {
						if ($project->title) {
							$proj2_attributes = $projectAssociations[$p]->attributes();
							$proj2_uuid = $proj2_attributes->uuid;							
							$project2_title = $project->title->children($namespaces['core']);
							$proj2_title = ($project_title->localizedString) ? $project2_title->localizedString : ""; 
						}		
					}									
					// insert project associations into table 
					$table_name = $wpdb->prefix . "pure_profiles_project_project_association";
					$exists = $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE (proj1_uuid='".$proj_uuid."' AND proj2_uuid='".$proj2_uuid."') OR (proj1_uuid='".$proj2_uuid."' AND proj2_uuid='".$proj_uuid."')" );
					if (count($exists) > 0) {
						continue; // exit current iteration only
					}
					$rows_affected = $wpdb->insert( $table_name, array( 
						'proj1_uuid' => $proj_uuid,
						'proj1_title' => $proj_title, 					
						'proj2_uuid' => $proj2_uuid, 
						'proj2_title' => $proj2_title, 
						'retrieved' => $retrieved
					));					
	 				
				}
			}	
			**/

		

			
			/* insert project into table */
			$table_name = $wpdb->prefix . "pure_profiles_project";
			$rows_affected = $wpdb->insert( $table_name, array( 
				'projectuuid' => $proj_uuid, 
				'title' => $proj_title,
				'portalurl' => $portal_url,
				'acronym' => $the_acronym,
				'description' => $the_description,
				'laymansdescription' => $the_laymans_description,
				'projecturl' => $the_url,
				'startdate' => $start_date,
				'finishdate' => $finish_date,
				'projectstatusid' => $status_id,
				'projectstatus' => $the_status,
				'typeclassificationid' => $type_classification_id,
				'typeclassification' => $the_type_classification,
				'fundingorganisation' => $funding_organisation,
				'keyfindings' => $the_key_findings,
				'parentproject' => $parent_project_uuid,
				'retrieved' => $retrieved
			));
			
			$table_name = $wpdb->prefix . "pure_profiles_project_keywords";		
			if ($project->keywordContainers) {
				$keywordContainers = $project->keywordContainers->children($namespaces['core']);
				$keywordContainerCount = count($keywordContainers);
				for ($kc=0; $kc < $keywordContainerCount; $kc++) {
					$keywordContainerAttributes = $keywordContainers[$kc]->attributes();
					$keywordContainerId = $keywordContainerAttributes->id;	
					$userDefinedKeywords = $keywordContainers[$kc]->children($namespaces['core'])->userDefinedKeyword;
					$userDefinedKeywordsCount = count($userDefinedKeywords);
					for ($udk=0; $udk < $userDefinedKeywordsCount; $udk++) {
						$userDefinedKeywordAttributes = $userDefinedKeywords[$udk]->attributes();
						$userDefinedKeywordId = $userDefinedKeywordAttributes->id;
						$keywords = $userDefinedKeywords[$udk]->children($namespaces['core']);
						$keywordsCount = count($keywords);
						if ($keywordsCount == 1) {
							$keyword = $keywords->freeKeyword;
							/* insert project keywords into table */
							$rows_affected = $wpdb->insert( $table_name, array( 
								'projectuuid' => $proj_uuid, 			
								'keywordcontainerid' => $keywordContainerId, 
								'userdefinedkeywordid' => $userDefinedKeywordId,
								'keyword' => $keyword,
								'retrieved' => $retrieved
							)); 						
						} else {
							for ($kw=0; $kw < $keywordsCount; $kw++) {
								$keyword = $keywords->freeKeyword[$kw];
								/* insert project keywords into table */
								$rows_affected = $wpdb->insert( $table_name, array( 
									'projectuuid' => $proj_uuid, 			
									'keywordcontainerid' => $keywordContainerId, 
									'userdefinedkeywordid' => $userDefinedKeywordId,
									'keyword' => $keyword,
									'retrieved' => $retrieved
								)); 
							}
						}
					}
				}
			}			
			
			if ($project->persons) {
				$project_persons = $project->persons->children($namespaces['person-template']);
				$num_project_persons = $project_persons->count();
						
				for ($j=0; $j < $num_project_persons; $j++) {
					if ($project_persons[$j]->person) {
						$person_attributes = $project_persons[$j]->person->attributes();
						$p_uuid = ($person_attributes['uuid']) ? (string)$person_attributes['uuid'] : "";
						$person_name = $project_persons[$j]->person->name->children($namespaces['core']);
						$first_name = $person_name->firstName;
						$last_name = $person_name->lastName;
            if ($project_persons[$j]->person->callName) {
              $call_name = $project_persons[$j]->person->callName->children($namespaces['core']);
              $call_name_first_name = $call_name->firstName;
              $call_name_last_name = $call_name->lastName;
            }
      
						$role_name = "";
						if ($project_persons[$j]->personRole) {
							$person_role_attributes = $project_persons[$j]->personRole->attributes();
							$person_role_id = (string)$person_role_attributes['id'];
							$person_role_details = $project_persons[$j]->personRole->children($namespaces['core']);
							$role_name = $person_role_details->term->localizedString;
						}									
									
						/* insert person into table */
						$table_name = $wpdb->prefix . "pure_profiles_project_person";
						$rows_affected = $wpdb->insert( $table_name, array( 
							'personuuid' => $p_uuid, 
							'projectuuid' => $proj_uuid,
							'title' => $proj_title,
							'firstname' => $first_name,
							'lastname' => $last_name,
							'callnamefirstname' => $call_name_first_name,
							'callnamelastname' => $call_name_last_name,
							'rolename' => $role_name,
							'retrieved' => $retrieved
						));				
					}

					
				}
			}
								
			$organisationAssociations = $project->organisations->children($namespaces['organisation-template']);
			$organisationAssociationsCount = count($organisationAssociations);		
			for ($j=0; $j < $organisationAssociationsCount; $j++) {
			
				$organisation = $organisationAssociations[$j]->children($namespaces['organisation-template']);

				if ($organisation->organisation) {
					$organisation_id = ($organisation->organisationId) ? $organisation->organisationId : "";
					$organisationAttributes = $organisationAssociations[$j]->organisation->attributes();
					$associated_organisation_uuid = $organisationAttributes->uuid;
					if ($organisation->organisation->name) {					
						$organisation_name = $organisation->organisation->name->children($namespaces['core']);
						$name = ($organisation_name->localizedString) ? (string)$organisation_name->localizedString : ""; 
					} else {
						$name = "";
					}
					if ($organisation->organisation->shortName) {					
						$organisation_short_name = $organisation->organisation->shortName->children($namespaces['core']);
						$short_name = ($organisation_short_name->localizedString) ? (string)$organisation_short_name->localizedString : ""; 
					} else {
						$short_name = "";
					}
					if ($organisation->organisation->typeClassification) {					
						$organisation_type_classification_attributes = $organisation->organisation->typeClassification->attributes();
						$org_type_classification_id = (string)$organisation_type_classification_attributes->id;
						$organisation_type_classification = $organisation->organisation->typeClassification->children($namespaces['core']);
						$org_type_classification = (string)$organisation_type_classification->term->localizedString;
					} else {
						$org_type_classification_id = "";
						$org_type_classification = "";
					}
				} else {
					if ($organisation->name) {
						$organisationAttributes = $organisationAssociations[$j]->attributes();
						$associated_organisation_uuid = $organisationAttributes->uuid;							
						$organisation_name = $organisation->name->children($namespaces['core']);
						$name = ($organisation_name->localizedString) ? $organisation_name->localizedString : ""; 
						$organisation_id = ($organisation->organisationId) ? $organisation->organisationId : "";
						$organisation_short_name = $organisation->shortName->children($namespaces['core']);
						$short_name = ($organisation_short_name->localizedString) ? $organisation_short_name->localizedString : "";
						$organisation_type_classification_attributes = $organisation->typeClassification->attributes();
						$org_type_classification_id = $organisation_type_classification_attributes->id;
						$organisation_type_classification = $organisation->typeClassification->children($namespaces['core']);
						$org_type_classification = $organisation_type_classification->term->localizedString;
					}		
				}

				
				// insert organisation associations into table 
				$table_name = $wpdb->prefix . "pure_profiles_project_organisation";
				$rows_affected = $wpdb->insert( $table_name, array( 
					'projectuuid' => $proj_uuid, 
					'title' => $proj_title, 
					'organisationuuid' => $associated_organisation_uuid,
					'organisationid' => $organisation_id,
					'organisationname' => $name, 
					'organisationshortname' => $short_name,
					'orgtypeclassification' => $org_type_classification, 
					'orgtypeclassificationid' => $org_type_classification_id,
					'retrieved' => $retrieved
				));

			}						

		}
		
	}
	
}