<?php

//if uninstall not called from WordPress exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )
	exit ();

global $wpdb;
if (is_multisite()) {

    $blogs = $wpdb->get_results("SELECT blog_id FROM ".$wpdb->base_prefix."blogs");
	
    if ($blogs) {
        foreach($blogs as $blog) {
            switch_to_blog($blog->blog_id);

            delete_option('pure_proxy_db_version');
            delete_option('pure_proxy_options');
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_person;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_person_information;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_organisation;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_organisation_information;");			
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_organisation_association;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_staff_organisation_association;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_keywords;");			
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_person;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_organisation;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_project_association;");	
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_publication_association;");	
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_document;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_keywords;");			
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_person;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_organisation;");	
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_publication_association;");	
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_clipping;");
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_clipping_reference;");				
            $wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_clipping_person;");			
					
        }
        restore_current_blog();
    }
} else {
	delete_option('pure_proxy_db_version');
	delete_option('pure_proxy_connection_options');
	delete_option('pure_proxy_parameters_options');
	delete_option('pure_proxy_cron_options');

	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_person;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_person_information;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_organisation;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_organisation_information;");				
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_organisation_association;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_staff_organisation_association;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_keywords;");			
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_person;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_organisation;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_project_association;");	
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_project_publication_association;");	
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_document;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_keywords;");	
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_person;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_organisation;");	
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_publication_publication_association;");		
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_clipping;");
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_clipping_reference;");				
	$wpdb->query("DROP TABLE IF EXISTS ".$wpdb->prefix."pure_profiles_clipping_person;");	
}




