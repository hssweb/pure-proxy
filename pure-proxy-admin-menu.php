<?php if(!defined('PUREPROXY_PATH')){die('Direct access not permitted');}

add_action( 'admin_menu', 'pure_proxy_menu' );
function pure_proxy_menu() {
	add_menu_page('Pure Proxy', 'Pure Proxy', 'manage_options', 'pure-proxy-menu', 'pure_proxy_connection_options', 'dashicons-id-alt');
	add_submenu_page( 'pure-proxy-menu', 'Pure Proxy - Connection Details', 'Connection Details', 'manage_options', 'pure-proxy-menu', 'pure_proxy_connection_options' ); 
	add_submenu_page( 'pure-proxy-menu', 'Pure Proxy - Parameters', 'Parameters', 'manage_options', 'pure-proxy-menu-parameters', 'pure_proxy_parameters_options' ); 		
	add_submenu_page( 'pure-proxy-menu', 'Pure Proxy - Cron', 'Cron', 'manage_options', 'pure-proxy-menu-cron', 'pure_proxy_cron_options' ); 	
}

function pure_proxy_connection_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Connection options updated.</p></div>';
	}
	$pure_proxy_options = array(
		'pure_proxy_host_settings'=> '', 
		'pure_proxy_username_settings' => '', 
		'pure_proxy_password_settings' =>''		
		);
	add_option('pure_proxy_connection_options', $pure_proxy_options);
?>	
	<div class="wrap">
	<h2>Pure Proxy</h2>
	<form action="options.php" method="post">				
		<?php settings_fields('pure_proxy_connection_options'); ?>
		<?php do_settings_sections('pure_proxy_connection'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" value="Save Changes" />
		<p></p>
		<?php $options = get_option('pure_proxy_connection_options'); ?>
	</form>	
	</div>
<?php						
}

function pure_proxy_parameters_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Parameters updated.</p></div>';
	}
	$pure_proxy_options = array(
		'pure_proxy_person_uuids_settings' => '',		
		'pure_proxy_org_uuids_settings' => ''
		);

?>	
	<div class="wrap">
	<h2>Pure Proxy</h2>
<?php
	add_option('pure_proxy_parameters_options', $pure_proxy_options);

	$options = get_option('pure_proxy_parameters_options');
?>	
	<form action="options.php" method="post">				
		<?php settings_fields('pure_proxy_parameters_options'); ?>
		<?php do_settings_sections('pure_proxy_parameters'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" value="Save Changes" />
		<p></p>
	</form>	
	</div>
<?php		
}
 
$cron_key = uniqid();
function pure_proxy_cron_options() {
	global $cron_key;
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Cron options updated.</p></div>';
	}
	$pure_proxy_options = array(	
		'pure_proxy_cron_key_settings' => $cron_key,
    'pure_proxy_cron_debug_settings' => $cron_debug
	);
	add_option('pure_proxy_cron_options', $pure_proxy_options);
?>	
	<div class="wrap">
	<h2>Pure Proxy</h2>
	<form action="options.php" method="post">				
		<?php settings_fields('pure_proxy_cron_options'); ?>
		<?php do_settings_sections('pure_proxy_cron'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" value="Save Changes" />
		<p></p>
		<?php $options = get_option('pure_proxy_cron_options'); ?>
	</form>	
	</div>
<?php		
}


add_action('admin_init', 'pure_proxy_admin_init');
function pure_proxy_admin_init(){
	global $pure_proxy_db_version, $pure_proxy_current_db_version;
	/* if tables do not exist, then create them (this is in admin init so that it works on multisite) */
	if (! get_option('pure_proxy_db_version')) {
		require_once(PUREPROXY_PATH.'/install.php');	
		pure_proxy_install();
	}
	// initialise options array
	$connection_options = get_option('pure_proxy_connection_options');
	$parameters_options = get_option('pure_proxy_parameters_options');
	$cron_options 		= get_option('pure_proxy_cron_options');
	
	/* check to see if any db updates need to be made */
	$pure_proxy_current_db_version = get_option('pure_proxy_db_version');
	if (version_compare($pure_proxy_current_db_version, $pure_proxy_db_version, '<')) {
		require_once(PUREPROXY_PATH.'/update.php');
		pure_proxy_version_update();
	}

	register_setting( 'pure_proxy_connection_options', 'pure_proxy_connection_options', 'pure_proxy_connection_options_validate' );
	register_setting( 'pure_proxy_parameters_options', 'pure_proxy_parameters_options', 'pure_proxy_parameters_options_validate' );
	register_setting( 'pure_proxy_cron_options', 'pure_proxy_cron_options', 'pure_proxy_cron_options_validate' );
	add_settings_section('pure_proxy_connection', '', 'pure_proxy_connection_section_text', 'pure_proxy_connection', 'pure_proxy_connection' );
	add_settings_field('pure_proxy_hostname', 'Hostname', 'pure_proxy_host_settings', 'pure_proxy_connection', 'pure_proxy_connection' );
	add_settings_field('pure_proxy_username', 'Username', 'pure_proxy_username_settings', 'pure_proxy_connection', 'pure_proxy_connection' );
	add_settings_field('pure_proxy_password', 'Password', 'pure_proxy_password_settings', 'pure_proxy_connection', 'pure_proxy_connection' );
	add_settings_section('pure_proxy_parameters', '', 'pure_proxy_parameters_section_text', 'pure_proxy_parameters', 'pure_proxy_parameters');
	add_settings_field('pure_proxy_person_uuids', 'Pure Person UUIDs', 'pure_proxy_person_uuids_settings', 'pure_proxy_parameters', 'pure_proxy_parameters' );
	add_settings_field('pure_proxy_org_uuids', 'Pure Organisation UUIDs', 'pure_proxy_org_uuids_settings', 'pure_proxy_parameters', 'pure_proxy_parameters' );
	add_settings_section('pure_proxy_cron', '', 'pure_proxy_cron_section_text', 'pure_proxy_cron', 'pure_proxy_cron' );
	add_settings_field('pure_proxy_cron_key', 'Key for Cron', 'pure_proxy_cron_key_settings', 'pure_proxy_cron', 'pure_proxy_cron' );
  add_settings_field('pure_proxy_cron_debug', 'Show debug info', 'pure_proxy_cron_debug_settings', 'pure_proxy_cron', 'pure_proxy_cron' );
}

function pure_proxy_connection_section_text() {
	echo '<h3>Connection Details</h3><p>Set the connection details for your Pure installation. </p>';
}

function pure_proxy_parameters_section_text() {
	echo '<h3>Parameters</h3><p>Specify the data you wish to retrieve from Pure. Specifying 
	an organisation\'s Pure UUID will retrieve all of the people associated with that organisation.</p>
		<p>UUIDs should be in a comma separated list. Line breaks are allowed and ignored.</p>
    <p>An example of a UUID is f81d4fae-7dec-11d0-a765-00a0c91e6bf6.</p>';
}

function pure_proxy_cron_section_text() {
	global $cron_key;
	$options = get_option( 'pure_proxy_cron_options' );
	$cron_key = ($options['pure_proxy_cron_key_settings'] != "") ? $options['pure_proxy_cron_key_settings'] : $cron_key;	
	$cron_link = plugins_url( 'cron.php?key='.$cron_key , __FILE__ ) ;
	
	echo '<h3>Cron</h3><p>You should set up a cron to update the local cache. For example, to update every day at 4am:</p>
		<code>0 4 * * * curl '.$cron_link.' >/dev/null 2>&1</code>';
	echo '<p>It is important that the script is <strong>not</strong> called during the day or between midnight and 3am.</p>';
}

function pure_proxy_host_settings() {
	$options = get_option( 'pure_proxy_connection_options' );
	$value = htmlentities ( $options['pure_proxy_host_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<input id='pure_proxy_host_settings' name='pure_proxy_connection_options[pure_proxy_host_settings]' size='50' type='text' value='{$value}' placeholder='e.g. http://www-beta.pure.ed.ac.uk' />";
}
function pure_proxy_username_settings() {
	$options = get_option('pure_proxy_connection_options');
	$value = htmlentities ( $options['pure_proxy_username_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<input id='pure_proxy_username_settings' name='pure_proxy_connection_options[pure_proxy_username_settings]' size='50' type='text' value='{$value}' placeholder='Enter username' />";
}
function pure_proxy_password_settings() {
	$options = get_option('pure_proxy_connection_options');
	$value = htmlentities ( $options['pure_proxy_password_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<input id='pure_proxy_password_settings' name='pure_proxy_connection_options[pure_proxy_password_settings]' size='50' type='password' value='{$value}' placeholder='Enter password' />";
}
function pure_proxy_person_uuids_settings() {
	$options = get_option('pure_proxy_parameters_options');
	$value = htmlentities ( $options['pure_proxy_person_uuids_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<textarea id='pure_proxy_person_uuids_settings' name='pure_proxy_parameters_options[pure_proxy_person_uuids_settings]' rows='4' cols='50'>{$value}</textarea>";
}
function pure_proxy_org_uuids_settings() {
	$options = get_option('pure_proxy_parameters_options');
	$value = htmlentities ( $options['pure_proxy_org_uuids_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<textarea id='pure_proxy_org_uuids_settings' name='pure_proxy_parameters_options[pure_proxy_org_uuids_settings]' rows='4' cols='50'>{$value}</textarea>";
}
function pure_proxy_cron_key_settings() {
	global $cron_key;
	$options = get_option('pure_proxy_cron_options');
	$value = htmlentities ( $options['pure_proxy_cron_key_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = $cron_key;
	}
	echo "<input id='pure_proxy_cron_key_settings' name='pure_proxy_cron_options[pure_proxy_cron_key_settings]' size='50' type='text' value='{$value}' />";
}
function pure_proxy_cron_debug_settings() {
	global $cron_debug;
	$options = get_option('pure_proxy_cron_options');
	$value = htmlentities ( $options['pure_proxy_cron_debug_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = $cron_debug;
	}
	echo "<input id='pure_proxy_cron_debug_settings' name='pure_proxy_cron_options[pure_proxy_cron_debug_settings]' size='50' type='checkbox' value='1' " . checked(1, $options['pure_proxy_cron_debug_settings'], false) . " />";
  echo "<label for='pure_proxy_cron_debug_settings'></label>";
}

function pure_proxy_connection_options_validate($input) {
	$options = get_option( 'pure_proxy_connection_options' );
	$options['pure_proxy_host_settings'] = trim($input['pure_proxy_host_settings']);
	$options['pure_proxy_username_settings'] = trim($input['pure_proxy_username_settings']);
	$options['pure_proxy_password_settings'] = trim($input['pure_proxy_password_settings']);	
	return $options;
}
function pure_proxy_parameters_options_validate($input) {
	$options = get_option( 'pure_proxy_parameters_options' );
	$options['pure_proxy_person_uuids_settings'] = trim($input['pure_proxy_person_uuids_settings']); 	
	$options['pure_proxy_org_uuids_settings'] = trim($input['pure_proxy_org_uuids_settings']); 			
	return $options;
}
function pure_proxy_cron_options_validate($input) {
	$options = get_option( 'pure_proxy_cron_options' );
	$options['pure_proxy_cron_key_settings'] = trim($input['pure_proxy_cron_key_settings']);	
	$options['pure_proxy_cron_debug_settings'] = intval($input['pure_proxy_cron_debug_settings']);
	return $options;
}
